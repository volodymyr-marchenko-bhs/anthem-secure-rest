package com.beaconhcs.ssl.securerest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import java.util.function.Function;

@RestController
public class EndpointController {

    private static final Logger logger = LoggerFactory.getLogger(EndpointController.class);

    @RequestMapping(path = "api/v1/test", method = RequestMethod.GET)
    public @ResponseBody String testEndpoint() {
        return "Welcome";
    }
}