package com.beaconhcs.ssl.securerest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurerestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurerestApplication.class, args);
    }

}
